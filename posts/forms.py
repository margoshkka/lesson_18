from django import forms
from .models import Post,Category


class AddCategoryForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = [
            'name',
            'description',
        ]


class EditCategoryForm(forms.ModelForm):
    def __init__ (self, *args, **kwargs):
        category = kwargs.pop('category')
        super(EditCategoryForm, self).__init__(*args, **kwargs)
        self.fields['description'].initial = category.description
        self.fields['name'].initial = category.name

    class Meta:
        model = Category
        fields = [
            'description',
            'name',
        ]


class AddPostForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = [
            'name',
            'description',
        ]


class EditCategoryForm(forms.ModelForm):
    def __init__ (self, *args, **kwargs):
        category = kwargs.pop('category')
        super(EditCategoryForm, self).__init__(*args, **kwargs)
        self.fields['description'].initial = category.description
        self.fields['name'].initial = category.name

    class Meta:
        model = Category
        fields = [
            'description',
            'name',
        ]