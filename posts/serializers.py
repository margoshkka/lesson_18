from rest_framework import serializers
from .models import Post, Category
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username', 'first_name', 'last_name', 'email', 'is_staff', 'is_active', 'date_joined']


class CategorySerializer(serializers.ModelSerializer):
    posts = serializers.PrimaryKeyRelatedField(many=True, queryset=Post.objects.all(), required=False)

    class Meta:
        model = Category
        fields = ('id', 'name', 'description', 'is_active')


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('id', 'status', 'category', 'user', 'title', 'content', 'created_on', 'updated_on')

