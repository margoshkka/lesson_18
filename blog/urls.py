"""blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf.urls import url
from posts.views import index,CategoryDetail, CategoriesList, PostsList, PostDetail

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    path('', index),
    url(r'^api/categories/$', CategoriesList.as_view()),
    url(r'^api/categories/(?P<pk>[0-9a-f\-]+)/$', CategoryDetail.as_view()),
    url(r'^api/posts/$', PostsList.as_view()),
    url(r'^api/posts/(?P<pk>[0-9a-f\-]+)/$', PostDetail.as_view()),
]
