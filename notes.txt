from rest_framework import serializers
from .models import Post, Category


class CategorySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    description = serializers.CharField(max_length=255,required=False)
    is_active = serializers.BooleanField(required=True)
    user = serializers.IntegerField(source='user_id',read_only=True)

    def create(self, validated_data):
        return Category.objects.create(**validated_data)

    def update(self,instance, validated_data):
        instance.description = validated_data.get('description', instance.description)
        instance.save()
        return instance


class PostSerializer(serializers.Serializer):

    id = serializers.IntegerField(read_only=True)
    status = serializers.ChoiceField(choices=Post.STATUSES,required=False)
    category = serializers.IntegerField(required=True, )
    user = serializers.IntegerField(source='user_id',read_only=True)
    title = serializers.CharField(max_length=255,required=True)
    content = serializers.CharField(max_length=255,required=True)
    created_on = serializers.DateTimeField(read_only=True)
    updated_on = serializers.DateTimeField(read_only=True)

    def create(self, validated_data):

        # validated_data['category']=validated_data['catego']
        return Post.objects.create(**validated_data)

    def update(self,instance, validated_data):
        print(validated_data)
        instance.title = validated_data.get('title', instance.title)
        instance.content = validated_data.get('content', instance.content)
        # instance.category_id =
        instance.category = Category.objects.get(pk=validated_data['category']['id'])
        instance.save()
        return instance


# use shell
# from posts.models import Post,Category
# from posts.serializers import PostSerializer,CategorySerializer
# post = Post.objects.first()
# serial = PostSerializer(post)
# serial.data
#
# serial_all = PostSerializer(posts, many=True)
#
#
# serial = PostSerializer(post, data={"title":"new_title"},partial=True)
# serial_category = CategorySerializer(Category.objects.first())
# serial = PostSerializer(post, data={"title":"new_title","content":"new_new_content","category":serial_category.data})
# serial.is_valid()
# serial.save()
